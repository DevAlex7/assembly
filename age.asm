start:

;mov d, 20  ; d= año actual
;sub d, 1   ; d -= año de nacimiento
;mov a, d   ; a = edad = d = 19 (en este caso)
;add a, 30  ; a += 30 = 49 --> ascii: 1

;mov b, 232 ; puntero al output
;mov [b], a ; imprime el 1er digito en el puntero del output
;add a, 8   ; a += 8 = 57 --> ascii: 9
;inc b      ; mover el puntero del output
;mov [b], a ; imprimir el 2do digito
